///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file list.hpp
/// @version 1.0
///
/// list header for animal farm 4 
///
/// @author eduardo kho jr <eduardok@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   3/25/21
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include"node.hpp"
namespace animalfarm{

class SingleLinkedList{
	public:
		const bool empty() const; // return true if the list is empty. false if there is anything in it
		void push_front ( Node* newNode ) ; // add newNode to the front of the list
		Node* pop_front(); // remove a node from the front of the list. if the list is already empty, return nullptr
		Node* get_first() const; // return the very first node from the list. Dont make any changes to the list.
		Node* get_next( const Node* currentNode ) const; // return the node immediately following currentNode.
		unsigned int size() const; // return the number of nodes in the list.

	protected:
		Node* head = nullptr;	

};
}


