///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file list.cpp
/// @version 1.0
///
/// list for animal farm 4 
///
/// @author eduardo kho jr <eduardok@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   3/25/21
///////////////////////////////////////////////////////////////////////////////
#include"list.hpp"
#include"node.hpp"
namespace animalfarm{

const bool SingleLinkedList::empty() const{

	if (head==nullptr){
	return true;
		}else{
	return false;
		}
	return 0;
} 

void SingleLinkedList::push_front(Node* newNode){   
	newNode->next = head;
	head = newNode;
	
}

Node* SingleLinkedList::pop_front(){
	
	if(head == nullptr){
	return nullptr;
	}
	Node* temp;
	temp = head;
	head = head -> next;
	return temp;

}

Node* SingleLinkedList::get_first() const{
	if(head == nullptr){
	return nullptr;
	}
	return head;
}

Node* SingleLinkedList::get_next( const Node* currentNode ) const {
	return currentNode->next;
}

unsigned int SingleLinkedList::size() const{
	int count = 0;
	Node* current = head;
	while ( current != 0 ){
		count++;
		current = current -> next;
	}	
	return count;
}








}
