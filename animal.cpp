///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author eduardo kho jr <eduardok@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   2/18/2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>
#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"


using namespace std;
using namespace animalfarm;
namespace animalfarm{
	

void Animal::printInfo() {
	cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}


string Animal::genderName (enum Gender gender) {
   switch (gender) {
	case MALE:    return string("Male"); break;
      	case FEMALE:  return string("Female"); break;
      	case UNKNOWN: return string("Unknown"); break;
   }

   return string("Really, really Unknown");
};
	

string Animal::colorName (enum Color color) {
   switch (color) {
	case BLACK:   return string("Black"); break;
	case WHITE:   return string("White"); break;
	case RED:     return string("Red");   break;
	case SILVER:  return string("Silver");break;
	case YELLOW:  return string("Yellow");break;
	case BROWN:   return string("Brown"); break;
	}
   return string("Unknown");
};

const enum Gender Animal::getRandomGender(){
   int a = rand()%3;
   switch(a){
	case 0: return MALE; break;
	case 1: return FEMALE; break;
	case 2: return UNKNOWN; break;
}
	return UNKNOWN;	
}

const enum Color Animal::getRandomColor(){
   int b = rand()%6;
   switch(b){
	case 0: return BLACK; break;
	case 1: return WHITE; break;
	case 2: return RED;   break;
	case 3: return SILVER;break;
	case 4: return YELLOW;break;
	case 5: return BROWN; break;
}
return BLACK;
}


const bool Animal::getRandomBool(){
   int c = rand()%2;
   switch(c){
	case 0: return false; break;
	case 1: return true;  break;
}
	return true;
}

const float Animal::getRandomWeight(const float from, const float to ){
   float randomWeight;
	 randomWeight = rand()%(int)to + (int)from;
	 return randomWeight;
}

const string Animal::getRandomName(){
   int length = rand()%12+1;
   char Consonents[] = {'b','c','d','f','g','h','j','k','l','m','n','p','q','r','s','t','v','w','x','z'};
   char Vowels[] = {'a','e','i','o','u','y'};
   char capConsonents[] = {'B','C','D','F','G','H','J','K','L','M','N','P','Q','R','S','T','V','W','X','Z'};
   char capVowels[] = {'A','E','I','O','U','Y'};
   
   string name = "";

   int random = rand() % 2;
   int count = 0;
   int random1 = rand() % 1;

   if (random1 == 1){
	name = name + capConsonents[rand() % 19]; 
   }else{
        name = name + capVowels[rand() % 19];
	}
   

   for(int i = 1; i < length; i++) {

   if(random < 2 && count < 2) {
      name = name + Consonents[rand() % 19];
      count++;
    }
    else {
      name = name + Vowels[rand() % 5];
      count = 0;
    }

    random = rand() % 2;

  }

   return name;

}

Animal* AnimalFactory::getRandomAnimal() {
	Animal *newAnimal = NULL;
	int z = rand() % 6;


	switch(z){
		case 0: newAnimal = new Cat (Animal::getRandomName(),Animal::getRandomColor(),Animal::getRandomGender());break;
		case 1: newAnimal = new Dog (Animal::getRandomName(),Animal::getRandomColor(),Animal::getRandomGender());break;
		case 2: newAnimal = new Nunu (Animal::getRandomBool(), RED,Animal::getRandomGender() );break;
		case 3: newAnimal = new Aku (Animal::getRandomWeight(0,45), SILVER, Animal::getRandomGender() );break;
		case 4: newAnimal = new Palila(Animal::getRandomName, YELLOW, Animal::getRandomGender() );break;
		case 5: newAnimal = new Nene ( Animal::getRandomBool(), BROWN, Animal::getRandomGender() );break;


}
		return newAnimal;
}

	Animal::Animal(void) {cout << '.';};
	Animal::~Animal(void) {cout << 'x';};
	

	} // namespace animalfarm

