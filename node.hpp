///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file node.hpp
/// @version 1.0
///
/// Test Main for animal farm 4 
///
/// @author eduardo kho jr <eduardok@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   3/25/21
///////////////////////////////////////////////////////////////////////////////

#pragma once
namespace animalfarm {

class Node{
	protected:
		Node* next = nullptr;
		friend class SingleLinkedList;
};





























}
