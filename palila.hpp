///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file palila.hpp
/// @version 1.0
///
/// Exports data about all palila
///
/// @author eduardo kho jr <eduardok@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   2/18/2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

#include "bird.hpp"

using namespace std;

namespace animalfarm {

class Palila : public Bird {
public:
	bool isMigratory;
	
	Palila( bool isMigratory ,enum Color newColor, enum Gender newGender );
	
	virtual const string speak();

	void printInfo();

	
};
}
